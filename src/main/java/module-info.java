/**
 * 
 */
/**
 * @author tim
 *
 */
module cli {
	exports de.grogra.cli;
	exports de.grogra.cli.ui;

	requires projectmanager;
	requires platform;
	requires platform.core;
	requires utilities;
	requires java.desktop;
	requires graph;
	requires xl.core;
	requires platform.swing;
	requires java.logging;
	requires org.jline;
	requires imp;
	requires imp3d;
	requires jdk.internal.le;
	requires java.prefs;
}
