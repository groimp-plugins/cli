package de.grogra.cli;

import java.io.StringWriter;

import org.jline.reader.LineReader;

public class CLIStringWritter extends StringWriter {
    private final LineReader reader;

    public CLIStringWritter(LineReader reader) {
        this.reader = reader;
    }

    @Override
    public void flush() {
        StringBuffer buffer = getBuffer();
        while (buffer.length() != 0) {
	        int lastNewline = buffer.lastIndexOf("\n");
	        if (lastNewline >= 0) {
	            reader.printAbove(buffer.substring(0, lastNewline + 1));
	            buffer.delete(0, lastNewline + 1);
	        }
	        else {
	        	reader.printAbove(buffer.toString());
	        	buffer.delete(0, buffer.length());
	        }
        }
    }
}