package de.grogra.cli;

import java.io.File;
import java.net.URL;
import java.nio.file.Path;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;

import de.grogra.cli.ui.*;
import de.grogra.graph.GraphState;
import de.grogra.graph.impl.Node;
import de.grogra.imp3d.View3D;
import de.grogra.pf.boot.Main;
import de.grogra.pf.io.FilterSource;
import de.grogra.pf.io.IO;
import de.grogra.pf.io.IOFlavor;
import de.grogra.pf.io.ObjectSourceImpl;
import de.grogra.pf.registry.Executable;
import de.grogra.pf.registry.Item;
import de.grogra.pf.registry.Registry;
import de.grogra.pf.registry.expr.ObjectExpr;
import de.grogra.pf.ui.Command;
import de.grogra.pf.ui.Context;
import de.grogra.pf.ui.FileChooserResult;
import de.grogra.pf.ui.JobManager;
import de.grogra.pf.ui.Panel;
import de.grogra.pf.ui.ProjectWorkbench;
import de.grogra.pf.ui.UI;
import de.grogra.pf.ui.UIApplication;
import de.grogra.pf.ui.Window;
import de.grogra.pf.ui.Workbench;
import de.grogra.pf.ui.registry.FileFactory;
import de.grogra.pf.ui.registry.SourceFile;
import de.grogra.projectmanager.ProjectManagerImpl;
import de.grogra.util.Map;
import de.grogra.util.MimeType;
import de.grogra.util.StringMap;
import de.grogra.util.ThreadContext;
import de.grogra.vfs.FileSystem;

public class CLIWorkbench extends ProjectWorkbench implements TreeModelListener {
	private Window window;
	// these presence of this field ensures that the graph
	// state is not garbage collected
	private GraphState regState;
	private Object token;

	public CLIWorkbench(Registry registry, JobManager jm, UIApplication app, Map initParams) {
		super(registry, jm, app.getUiToolKit(), initParams);
		this.app = app;
		Executable.runExecutables(getRegistry().getRootRegistry(), "/hooks/cliLaunch", getRegistry(),
				new StringMap());
	}

	public String toString() {
		if (getName() != null) {
			return getName();
		}
		return super.toString();
	}

	public void open(Object info) {
		try {
			app.open(info);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public Workbench open(FilterSource fs, Map initParams) {
		getApplication().open(fs, initParams);
		return null;
	}

	

	public boolean saveAs(Object info) {
		String path = Utils.getStringParameter(info, true);
		path = (path == null) ? null : Utils.getAbsolutePath(this, path).toString();
		FileChooserResult fr = app.getUiToolKit().chooseFile("save project", IO.getWritableFileTypes(IOFlavor.REGISTRY), Window.SAVE_FILE, false, null, this, path);
		
//		Path dest = Utils.getAbsolutePath(this, Utils.getStringParameter(info,false));
//		File f = dest.toFile();
//		MimeType m = IO.getMimeType(dest.toString());
//		
		if(fr!=null) {
			return saveAs(fr.file, fr.getMimeType());
		}
		return false;
	}

	public void listFunctions(Object info) {
		String erg = "";
		for (Command c : getFunctions()) {
			erg += System.lineSeparator()+ " - " + c.getCommandName();
		}
		((CLIWindowSupport) window).consoleWrite(erg);
	}


	/**
	 * info name of the file -
	 */
//	TODO: to be removed & use uitoolkit - not so sure
	public String getFileContent(Object info) {
		String name = Utils.getStringParameter(info,false);
		return name;
	}

	public void getProjectInfo(Object info) {
		((CLIWindowSupport) window).consoleWrite(getProject().toString());
	}
	
	@Override
	public String getApplicationName() {
		return "GroCLIMP";
	}

	@Override
	public Object getToken() {
		return token;
	}

	@Override
	public void setToken(Object token) {
		this.token = token;
	}

	
	@Override
	public Window getWindow() {
		return window;
	}

	@Override
	public Workbench getMainWorkbench() {

		return app.getMainWorkbench();
	}

	@Override
	public void stopLocalFileSynchronization() {
		// TODO Auto-generated method stub
	}

	@Override
	public void startLocalFileSynchronization() {
		// TODO Auto-generated method stub
	}

	void disposeWhenNotInitialized() {
		getRegistry().dispose();
//		app.getWorkbenchManager().deregisterWorkbench(this);
//		ProjectManagerImpl.getInstance().disconnectProject(this);
	}

	@Override
	public void dispose(Command afterDispose) {
		if (window != null) {
			window.dispose();
		}
		getRegistry().removeFileSystemListener(this);
		disposeWhenNotInitialized();
		if (afterDispose != null) {
			afterDispose.run(null, this);
		}
		setCurrent(null);
		window = null;
	}

	public void initialize() {
		ThreadContext s = getJobManager().getThreadContext();
		regState = getRegistry().getRegistryGraph().createStaticState(s);

		setCurrent(this);
		getRegistry().startup();
		getRegistry().addFileSystemListener(this);
		updateName();

		Map propMap = Map.EMPTY_MAP;
		Object o = getRegistry().getRootRegistry().getUserProperty(Main.SCREEN_PROPERTY_ID);
		if (o != null) {
			propMap = new StringMap().putInt(Main.SCREEN_PROPERTY, (Integer) o);
		}

		window = getToolkit().createWindow(CLOSE, propMap);
		if (window != null) {
			initializeWindow();
			window.show(true, null);
		}

		StringMap m = new StringMap().putObject("registry", getRegistry());
		m.putObject("workbench", this);
		m.putObject("filesystem", this.getRegistry().getFileSystem());
		Executable.runExecutables(getRegistry().getRootRegistry(), "/hooks/projectloaded", getRegistry(), m);
	}

	void initializeWindow() {
		initializeWindow(window);
	}

	public boolean isSelected() {
		return this == app.getCurrentWorkbench();
	}

	@Override
	protected void close(Command afterDispose) {
		
		if (getWindow() != null) {
			final Panel[] panels = getWindow().getPanels(null);
			new Command() {
				public void run(Object info, Context ctx) {
					for (int i = 0; i < panels.length; i++) {
						Panel p;
						if ((p = panels[i]) != null) {
							panels[i] = null;
							p.checkClose(this);
							return;
						}
					}
					close0(afterDispose);
				}

				public String getCommandName() {
					return null;
				}
			}.run(null, this);
		} else {
			close0(afterDispose);
		}
	}

	void close0(Command afterDispose) {
		if (isModified() && (getWindow() != null)) {
			int res = getWindow().showDialog(UI.I18N.msg("project.savequestion.title"),
					UI.I18N.msg("project.savequestion.msg", getName()), Window.QUESTION_CANCEL_MESSAGE);
			if (res == Window.CANCEL_RESULT) {
				return;
			} else if (res == Window.YES_OK_RESULT) {
				if (!save(true)) {
					return;
				}
			}
		}
		Registry r = getRegistry().getRootRegistry();
		Executable.runExecutables(r, "/hooks/close", r, UI.getArgs(this, null));
		getApplication().getWorkbenchManager().closeWorkbench(this);
		//		getJobManager().stop(afterDispose);
	}

	@Override
	public void treeNodesInserted(TreeModelEvent e) {
		treeStructureChanged(e);
	}

	@Override
	public void treeNodesRemoved(TreeModelEvent e) {
		treeStructureChanged(e);
	}

	@Override
	public void treeNodesChanged(TreeModelEvent e) {
		treeStructureChanged(e);
	}

	@Override
	public void treeStructureChanged(TreeModelEvent e) {
		if (!((FileSystem) e.getSource()).isPersistent()) {
			setModified();
		}
	}

	@Override
	public void addNode(Object info) {
		URL u = null;
		try {
			String link = Utils.getStringParameter(info, false);
			if (link.equals("-o")) {
				addGeometricalObject(info);
				return;
			} else {
				Path src = Utils.getAbsolutePath(this, link);
				if (src.toFile().exists()) {
					u =  de.grogra.util.Utils.fileToURL(src.toFile());
				} else {
					((CLIWindowSupport) window).consoleWrite(CLIApplication.I18N.msg("error.addnode.filenotfound"));

				}
			}
		} catch (NullPointerException e) {
		}
		FileFactory ex = (FileFactory) Item.resolveItem(getProject(), "/objects/objects/file");
		StringMap x = new StringMap();
		addNode((Node) ex.addFromURL(getProjectRegistry(), u, x, this));
	}
		
		
	
	private void addGeometricalObject(Object info) {
		try {
			String type=Utils.getStringParameter(info,false);
			Item i=Item.resolveItem(getProject(), "/objects/3d/geometry/"+type);
			if(i!=null) {
				ObjectExpr ob = (ObjectExpr)i;
				addNode((Node)ob.evaluate(getProjectRegistry(), new StringMap()));
			}
		}catch(NullPointerException e){
			((CLIWindowSupport) window).consoleWrite(CLIApplication.I18N.msg("error.object.add.missingargument"));	
		}
	}

	@Override
	public void addSourceFile(Object info) {
		String path = Utils.getStringParameter(info, true);
		path = (path == null) ? null : Utils.getAbsolutePath(this, path).toString();
		FileChooserResult fr = app.getUiToolKit().chooseFile(null,
				IO.getReadableFileTypes(new IOFlavor[] { IOFlavor.RESOURCE_LOADER }), Window.ADD_FILE, false,
				null, this, path);
		if (fr != null) {
			String dest = Utils.getStringParameter(info, true);
			addSourceFile(fr.file, fr.getMimeType(), dest);
		}
	}

	@Override
	public void export(Object info) {

		((Command) Item.resolveItem(this, "/ui/panels/3d/defaultview")).run(null, this);
		View3D view = (View3D) this.getWindow().getPanel("/ui/panels/3d/defaultview");
		if (view != null) {
			FilterSource src = new ObjectSourceImpl(view, "view", view.getFlavor(), getRegistry().getRootRegistry(),
					null);

			// try to receive the path from a parameter
			String path = Utils.getStringParameter(info, true);
			// if path existed get absolute path
			path = (path == null) ? null : Utils.getAbsolutePath(this, path).toString();
			// open File chooser with the path (if path is null dialog is opend)
			FileChooserResult fr = app.getUiToolKit().chooseFile(
					UI.I18N.getString("filedialog.exportfile", "Export Scene in file"),
					IO.getWritableFileTypes(src.getFlavor()), Window.OPEN_FILE, false, null, this, path);
			if (fr != null) {
				File file = fr.file;
				setProperty(EXPORT_VISIBLE_LAYER, false);
				export(src, fr.getMimeType(), file);
			}
		}
	}

	@Override
	public void renameSourceFile(Object info) {
		try {
			String file=Utils.getStringParameter(info,false);
			String name=Utils.getStringParameter(info,false);
			renameSourceFile(file, name);
		}catch(NullPointerException e) {
			((CLIWindowSupport) window).consoleWrite(CLIApplication.I18N.msg("error.file.rename.missingargument"));
		}
	}
	
	@Override
	public void removeSourceFile(Object info) {
		try {
			String file=Utils.getStringParameter(info,false);
			removeSourceFile(file);
		}catch(NullPointerException e) {
			
		}
	}
	
	
	public static void logTest(Item item, Object info, Context ctx) {
		ctx.getWorkbench().logInfo("whats up");
		//log(new Throwable("whats up!!!"));		
	}

	@Override
	public void execute(Object info) {
		try {
		String command=Utils.getStringParameter(info,false);
		execute(command);
		}catch(NullPointerException e) {
			((CLIWindowSupport) window).consoleWrite(CLIApplication.I18N.msg("error.rgg.run.missingargument"));
			listFunctions(info);
		}
	}
	
}
