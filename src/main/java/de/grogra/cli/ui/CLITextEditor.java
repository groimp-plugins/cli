package de.grogra.cli.ui;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.nio.file.Files;
import java.nio.file.attribute.BasicFileAttributes;

import de.grogra.cli.CLIApplication;
import de.grogra.cli.Utils;
import de.grogra.cli.ui.CLIAttributeEditorPanel.EditableAttribute;
import de.grogra.pf.ui.TextEditor;

public class CLITextEditor extends CLIPanelSupport implements TextEditor{

	public CLITextEditor() {
		super(new CLITextComponent());
		// TODO Auto-generated constructor stub
	}

	@Override
	public void openDocument(String systemId, String reference) {
		Object f = getWorkbench().getProjectRegistry().getProjectFile(systemId);
		OutputStream outStream=null;
		OutputStream out=null;
		InputStream backIn=null;
		try {
			InputStream ins=getWorkbench().getProjectRegistry().getFileSystem().getInputStream(f);
			File tf = File.createTempFile("groimpclitemp", "."+Utils.getExtension(systemId));
			BasicFileAttributes attributes_before = Files.readAttributes(tf.toPath(), BasicFileAttributes.class);
			outStream = new FileOutputStream(tf);   
			outStream.write(ins.readAllBytes());
			outStream.close();
			CLIApplication app = ((CLIApplication)getWorkbench().getApplication());
			if (app.canReadValue()) {
				app.setFileToEdit(tf, systemId);
				app.startConsoleFileEditor();
				if(tf.exists()) {  // while using a temporary file there is the posibillity that the system is cleaning the tmp folder while the file was open...
					BasicFileAttributes attributes_after = Files.readAttributes(tf.toPath(), BasicFileAttributes.class);					
					//check if the file changed 
					if(!attributes_before.lastModifiedTime().equals(attributes_after.lastModifiedTime())) {
						backIn = new FileInputStream(tf);
						out=getWorkbench().getProjectRegistry().getFileSystem().getOutputStream(f,false);
						out.write(backIn.readAllBytes());
						out.close();
						backIn.close();
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}finally {
			try {
				outStream.close();
				out.close();
				backIn.close();
			} catch (IOException e) {}
			
		}
	}
	
	public List<String> openAttributeEditor(Map<String, EditableAttribute> widgetList) {
		List<String> outputLines = new ArrayList<String>();
		CLIApplication app = ((CLIApplication)getWorkbench().getApplication());
		if (app.canReadValue()) {
		BufferedWriter bf = null;
		try {
			File tf = File.createTempFile("groclimptemp-", "-attributeeditor");
			bf = new BufferedWriter( new FileWriter(tf));
				
			for (Map.Entry<String, EditableAttribute> entry : widgetList.entrySet()) {
				bf.write(entry.getKey()+':'+ entry.getValue());
				bf.newLine();
			}		
			bf.close();
			app.setFileToEdit(tf,"");
			app.startConsoleFileEditor();
		    try (BufferedReader br= 
		    		new BufferedReader(new InputStreamReader(new FileInputStream( tf)))) {
		        String line;
		        while ((line = br.readLine()) != null) {
		        	outputLines.add(line);
		        }
		    }
		}catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
                bf.close();
            }
            catch (Exception e) {}
		}
		}
		return outputLines;
	}
	
	
	@Override
	public String[] getDocuments() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void closeDocument(String systemId) {
		// TODO Auto-generated method stub
		
	}

}
