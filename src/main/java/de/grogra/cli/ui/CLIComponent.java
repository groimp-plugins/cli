package de.grogra.cli.ui;

import de.grogra.cli.ExecutableComponent;
import de.grogra.pf.ui.ComponentWrapper;

public class CLIComponent implements ComponentWrapper, ExecutableComponent{
	
	private String name;
	private Object content;
	transient CLIContainer parent;
	private boolean showable=false;
	private boolean visible=false;
	
	static final Object LOCK = new TreeLock();
    static class TreeLock {}

    public CLIComponent() {
    	this.name = "default_name";
    }
    
    public CLIComponent(String name) {
    	this.name=name;
    }

	@Override
	public void dispose() {
	}

	@Override
	public Object getComponent() {
		return this;
	}
	
	public final Object getTreeLock() {
        return LOCK;
    }

    final void checkTreeLock() {
        if (!Thread.holdsLock(getTreeLock())) {
            throw new IllegalStateException("This function should be called while holding treeLock");
        }
    }
    
    // Should only be called while holding tree lock
    int countHierarchyMembers() {
        return 1;
    }
    
    public CLIContainer getParent() {
    	return parent;
    }

	public void show() {
		if (isShowable()) {
			System.out.println("Component: "+name);
			System.out.println(content);
		}
	}
	
	public boolean isShowable() {
		return showable;
	}

	public void setShowable(boolean s) {
		showable=s;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public Object getContent() {
		return content;
	}
	
	public void setContent(Object content) {
		this.content = content;
	}
	
	public boolean getVisible() {
		return visible;
	}
	
	public void setVisible(boolean v) {
		this.visible = v;
	}
	
//	public boolean isVisible() {
//		return visible;
//	}
	
	public CLIPanelSupport getSupport() {
		return getRootParent().getSupport();
	}
	
	public CLIComponent getRootParent() {
		CLIComponent c = this;
		while(c.getParent()!=null)
			c = c.getParent();
		return c;
	}

	@Override
	public void run(String s, Object info) {
		switch (s) {
		case("display"):
			setVisible(true);
			break;
		case("hide"):
			setVisible(false);
			break;
		case("repaint"):
			show();
		}
	}
}
