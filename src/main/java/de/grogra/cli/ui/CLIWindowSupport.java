package de.grogra.cli.ui;

import java.awt.HeadlessException;
import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.swing.filechooser.FileFilter;

import de.grogra.cli.CLIApplication;
import de.grogra.pf.io.IO;
import de.grogra.pf.ui.Command;
import de.grogra.pf.ui.ComponentWrapper;
import de.grogra.pf.ui.FileChooserResult;
import de.grogra.pf.ui.Panel;
import de.grogra.pf.ui.UI;
import de.grogra.pf.ui.UIProperty;
import de.grogra.pf.ui.UIToolkit;
import de.grogra.pf.ui.Window;
import de.grogra.pf.ui.Workbench;
import de.grogra.pf.ui.registry.Layout;
import de.grogra.pf.ui.tree.UITree;
import de.grogra.util.Disposable;
import de.grogra.util.I18NBundle;
import de.grogra.util.Map;
import de.grogra.xl.lang.ObjectToBoolean;

public class CLIWindowSupport extends CLIPanelSupport implements Window {
	
	CLIRootPane frame;
	UIToolkit manager;
	Workbench workbench;
	boolean visible;
	
	private volatile Panel[] windowsToDispose;
	private volatile List<Panel> panels = new ArrayList<Panel>();

	private final Command close;
	
	public CLIWindowSupport (UIToolkit manager, Command close, Map params)
	{
	
		super(new CLITextComponent());
		this.close = close;
		this.frame = (CLITextComponent) getComponent();
		this.manager = manager;
		initialize (this, params);
		
	}
	
	public void addPanel(Panel p) {
		panels.add(p);
	}
	
	public void removePanel(int panelNb) {
		panels.remove(panelNb);
	}
	
	public void removePanel(String panelId) {
		panels.remove(getPanelNb(panelId));
	}
	
	public int getPanelNb(String panelId){
		return panels.indexOf(getPanel(panelId));
	}
	
	/**
	 * Push content into the frame (which is a CLITextComponent)
	 * Only take String for now
	 */
	public void consoleWrite(ComponentWrapper c) {
		((CLITextComponent)frame).read((String)c.getComponent().toString());
	}

	public void consoleWrite(String s) {
		((CLITextComponent)frame).read((s));
	}

	/**
	 * Return the frame of the windows, which is a CLITextComponent for the CLIWindowSupport 
	 * to display output text
	 */
	@Override
	public ComponentWrapper getContent() {
		return (ComponentWrapper) frame;
	}


	@Override
	public void setCursor(int cursor) {	}

	@Override
	public Window getWindow() {
		return this;
	}

	@Override
	public Workbench getWorkbench() {
		return workbench;
	}

	public void initializeWorkbench (Workbench workbench)
	{
		assert this.workbench == null;
		this.workbench = workbench;
		panelId=workbench.getName()+".mainConsole";
	}
	
	/**
	 * Check if the window is currently displayed (i.e. if its workbench is currently selected)
	 * The components also have a visible parameter (get with getVisible() ).
	 */
	@Override
	public boolean isVisible() {
		return getWorkbench().isSelected()|| getWorkbench().getMainWorkbench()==getWorkbench();
	}

	/**
	 * Get all the open panels managed by this window.
	 */
	@Override
	public Panel[] getPanels(ObjectToBoolean<Panel> filter) {
		return panels.toArray(new Panel[0]);
	}

	/**
	 * Get a Panel managed by this window (it has be be opened) from its ID.
	 * It open the panel (i.e. commands are now pushed into the panel until it is closed or hided)
	 */
	@Override
	public Panel getPanel(String panelId) {
		for (Panel p : panels) {
			if(p.resolve().getPanelId() != null &&
					p.resolve().getPanelId().equals(panelId)) {
				return ((CLIPanelSupport)p).unresolve ();
			}
		}
		return null;
	}
		
	@Override
	void setVisibleSync (boolean visible, Panel keepInFront, boolean moveToFront)
	{
		if (visible)
		{
			if (frame != null)
			{
//				frame.setVisible(true);
				frame.show();
			}
		}
		else
		{
			if (frame != null)
			{
//				(() frame).hide();
			}
		}
	}

	@Override
	public void setLayout(Layout layout, de.grogra.util.Map params) {}

	@Override
	public Layout getLayout() {
		return null;
	}
	
	@Override
	public void setMenuVisibility(boolean value) {
	}

	
	/**
	 * Create a "dialog" (a request in the terminal) to get a path to a file.
	 * @param title: the message displayed
	 * @param directory: the base directory for the path
	 * @param filters: the list of accepted filters for the file type
	 * @param type: possible types from @link de.grogra.pf.ui.Window types: OPEN_FILE = 0; ADD_FILE = 1; SAVE_FILE = 2;
	 * @param mustExist: the existance of the file is tested if true and return an error if its not the case.
	 * @param selectedfilter: ?? Never used in GroIMP (always set as null) probably the default filter selected in the dialog
	 */
	// ALL below should be in uitoolkit
	@Override
	public FileChooserResult chooseFile(String title, File directory, FileFilter[] filters, int type, boolean mustExist,
			FileFilter selectedFilter) {
		
		CLIApplication app = ((CLIApplication)getWorkbench().getApplication());
		if ( app.canReadValue()) {
			FileChooserResult r = new FileChooserResult ();
			
			String prompt = "["+((directory!=null)? directory.toString(): "/")+"]>";
			String msg = title;
			for (FileFilter f : filters) {
				msg+="; "+f.getDescription();
			}

			consoleWrite(msg.toString());
			app.setPROMPT(prompt);
			String t = (String) app.getTerminalInput();
			if (t==null) {
				r.file = null;
			}
			else if (Path.of(t).isAbsolute()){
				File file = new File(t);
				r.file=file;
			}
			else {
				r.file = Paths.get(directory.toString(), t).toFile();
			}
			r.filter = filters[0];
			
			return r.validate (mustExist, type);
		}
		consoleWrite(CLIApplication.I18N.msg("error.fs.notlocked"));
		return null;
	}

	/**
	 * Create a dialog where one of the options get selected. The option selected return its index in the options param.
	 * @param title not used here
	 * @param bundle : a bundle from which to load text
	 * @param keyBase : the key from the bundle to the message displayed
	 * @param options : an array of options for the user to pick from. 
	 */
	@Override
	public int showChoiceDialog(String title, I18NBundle bundle, String keyBase, String[] options) {
		
		CLIApplication app = ((CLIApplication)getWorkbench().getApplication());
		if ( app.canReadValue()) {
			
			int i = 0;
			String msg = bundle.msg(keyBase) + System.lineSeparator();
			for (String s : options) {
				msg+= String.valueOf(i) + " :"+s;
				i++;
			}
			
			String prompt = "[pick an index]>";
			
			consoleWrite(msg.toString());
			app.setPROMPT(prompt);
			String t = (String) app.getTerminalInput();
			if (Integer.valueOf(t)!=null && Integer.valueOf(t)>=0 && Integer.valueOf(t)<=options.length){
				return Integer.valueOf(t);
			}
			else {return Window.CANCEL_RESULT;}
		}
		consoleWrite(CLIApplication.I18N.msg("error.fs.notlocked"));
		return Window.CANCEL_RESULT;
	}

	@Override
	public int showDialog(String title, Object message, int type) {

		CLIApplication app = ((CLIApplication)getWorkbench().getApplication());
		if ( app.canReadValue()) {
			consoleWrite(message.toString()+System.lineSeparator());
			app.setPROMPT("> ");
			String t = (String) app.getTerminalInput();
			if (t==null) {return Window.CANCEL_RESULT;}
			if (t.toLowerCase().equals("yes") || t.toLowerCase().equals("y"))
				return Window.YES_OK_RESULT;
			if (t.toLowerCase().equals("no") || t.toLowerCase().equals("n"))
				return Window.NO_RESULT;			
			try {
				if (Integer.valueOf(t) == 0) {
					return Window.YES_OK_RESULT;
			}
			}catch(NumberFormatException e) {}
		}
		return Window.CANCEL_RESULT;
	}

	@Override
	public String showInputDialog(String title, Object message, String initial) {
		CLIApplication app = ((CLIApplication)getWorkbench().getApplication());
		if ( app.canReadValue()) {
			consoleWrite(message.toString()+System.lineSeparator());
			app.setPROMPT("> ");
			String t = (String) app.getTerminalInput();			
			return t;
		}
		return null;
	}

	@Override
	public Disposable showWaitMessage(String toComplete) {
		CLIApplication app = ((CLIApplication)getWorkbench().getApplication());
		String msg = (toComplete != null) ? UI.I18N.msg (
				"waitingmessage.text", toComplete) : UI.I18N
				.msg ("waitingmessage.text0");
		consoleWrite(msg.toString()+System.lineSeparator());
		return null;
	}

	
}
