package de.grogra.cli.ui;

import de.grogra.pf.ui.tree.UITree;
import de.grogra.util.Disposable;
import de.grogra.util.Map;

public interface ICLIPanel extends Disposable
{
	CLIPanelSupport getSupport ();

	void initialize (CLIPanelSupport support, Map params);

	void setMenu (CLIComponent menu);

	CLIContainer getContentPane ();
	
	void setContentPane(CLIContainer c);
}

