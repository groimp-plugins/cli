package de.grogra.cli.ui;

import java.util.AbstractQueue;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

import de.grogra.cli.CLIApplication;
import de.grogra.pf.ui.Showable;
import de.grogra.pf.ui.Workbench;

/**
 * A simple text panel that can take string data as input & output.
 * It also "log" all output data of a workbench as a queue that can be displayed in the terminal when
 * the workbench is selected
 */
public class CLITextComponent extends CLIRootPane{
	
	private final Object LOCK = new Object();
	private boolean repaint;
	
	class TextQueue<T> extends AbstractQueue<T> {

	    private LinkedList<T> elements;

	    public TextQueue() {
	      this.elements = new LinkedList<T>();
	    }

		@Override
		public boolean offer(T e) {
			if(e == null) return false;
		    elements.add(e);
		    return true;
		}

		@Override
		public T poll() {
			Iterator<T> iter = elements.iterator();
		    T e = iter.next();
		    if(e != null){
		        iter.remove();
		        return e;
		    }
		    return null;
		}

		@Override
		public T peek() {
			return elements.getFirst();
		}

		@Override
		public Iterator<T> iterator() {
			return elements.iterator();
		}

		@Override
		public int size() {
			return elements.size();
		}

	}
	
	
	private TextQueue<String> history;
	private TextQueue<String> toDisplay;
	
	
	/**
	 * The component print down its content
	 * Push one line from the log queue to be printed
	 */
	public String write() {
		String el = toDisplay.poll();
		if (el!=null) {
			history.add(el+System.lineSeparator());
			return el;
		}
		return null;
	}

	/**
	 * Created to use the Console in the API
	 * @return
	 */
	
	public Object[] writeAll() {
		ArrayList<String> content=new ArrayList<String>();
		while (!toDisplay.isEmpty()) {
			content.add(write());
		}
		return content.toArray();			
		
	}
	
	/**
	 * The component read some String - 
	 * Add an object to the log queue
	 * Return true if something was added to the queue
	 */
	public boolean read(String e) {
		synchronized (LOCK) {
		if (e!=null) {
			e=removeBreakLine(e);
			if (!e.equals("> ") && !e.equals("")) { // remove some "ugly" chars pushed by other panels
			toDisplay.add(e);
			getSupport().show(true, null);
			return true;
		}
		}
		return false;
		}
	}
	
	/**
	 * This additional breakline method smooth the break lines in from read
	 */
	public void breakLine() {
		synchronized (LOCK) {
			toDisplay.add(System.lineSeparator());
			show();
		}
	}
	
	private String removeBreakLine(String e) {
		if (e.endsWith(System.lineSeparator())) {
			e=e.substring(0, e.length() - System.lineSeparator().length());
		}
		return e;
	}
	
	
	public CLITextComponent() {
		
		setName("text component");
		setShowable(true);
		setVisible(true);
		toDisplay = new TextQueue<>();
		history = new TextQueue<>();
		setContent(toDisplay);
	}
		
	private CLIWindowSupport getWindow() {
		return (CLIWindowSupport) getSupport().getWindow();
	}
	
	@Override
	public void show() {
		if(getVisible()) {
			if ( getWindow().isVisible()) {
				repaint=false;
				while (!toDisplay.isEmpty()) {
					repaint=true;
					((CLIApplication)getWindow().getWorkbench().getApplication()).print(write());
				}
				if(repaint) {
					((CLIApplication)getWindow().getWorkbench().getApplication()).flush();
				}
			}
		}
	}
	
	@Override
	public void run(String cmd, Object info) {
		super.run(cmd,info);
		switch (cmd) {
		case("history"):
			if (getWindow().isVisible()) {
				repaint=false;
				for(String s : history) { 
					repaint=true;
					((CLIApplication)getWindow().getWorkbench().getApplication()).print(s.toString()); 
					}
				if(repaint) {
					((CLIApplication)getWindow().getWorkbench().getApplication()).flush();
				}
			}
			break;
		}
	}
}
