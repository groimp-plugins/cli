package de.grogra.cli.ui;

import de.grogra.util.Disposable;
import de.grogra.util.Map;

public class CLIPanel extends CLIRootPane implements ICLIPanel{

	CLIPanelSupport ps;
	private Disposable toDispose;
	public Disposable tag;
	CLIComponent menu;
	
	public CLIPanel(Disposable toDispose) {
		this.toDispose = toDispose;
	}
	
	@Override
	public CLIPanelSupport getSupport() {
		return ps;
	}

	public void dispose() {
		if (toDispose != null)
		{
			toDispose.dispose ();
			toDispose = null;
		}
	}
	
	@Override
	public void initialize(CLIPanelSupport support, Map params) {
		this.ps = support;
	}

	@Override
	public void setMenu(CLIComponent menu) {
		this.menu = menu;
	}


}
