package de.grogra.cli.ui;

import java.awt.EventQueue;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import de.grogra.cli.CLIApplication;


public class CLIStatusBar extends CLIRootPane implements PropertyChangeListener {

	String label;
	int avgTime;

	public CLIStatusBar ()
	{
		super ();
	}


	@Override
	public void initialize (CLIPanelSupport support, de.grogra.util.Map p)
	{
		super.initialize (support, p);
		support.getWorkbench ().addStatusChangeListener (this);
	}


	@Override
	public void dispose ()
	{
		getSupport ().getWorkbench ().removeStatusChangeListener (this);
	}


	private long lastTime;
	
	private float lastProgressBarValue = 2f;
	private long startTime;

	public void propertyChange (final PropertyChangeEvent event)
	{
		if ("progress".equals (event.getPropertyName ()))
		{
			Float f = (Float) event.getNewValue ();
			if ((f != null) && (f.floatValue () >= 0))
			{
				long t = System.currentTimeMillis ();
				if ((t - lastTime) < 8 * avgTime)
				{
					return;
				}
				lastTime = t;
			}
		}
		Runnable r = new Runnable ()
		{
			public void run ()
			{
				if ("status".equals (event.getPropertyName ()))
				{
					String s = (String) event.getNewValue ();
					label = (((s == null) || s.equals (""))
								   ? " " : s);
				}
				else if ("progress".equals (event.getPropertyName ()))
				{
					Float f = (Float) event.getNewValue ();
					if (f != null)
					{
						if (f.floatValue () < 0)
						{
						}
						else
						{
							if (lastProgressBarValue > f.floatValue()) {
								// a new process, reset the start time
								startTime = lastTime;
								lastProgressBarValue = f.floatValue();								
							} else {
								long remaining = (long) (((lastTime - startTime) / f.floatValue()) - (lastTime - startTime)) / 1000;
								if (remaining < 60 ) {
									updateProgress("Remaining: " + remaining + " sec");
								} else if (remaining < 3600) {
									updateProgress("Remaining: " + remaining / 60 + " min");
								} else {
									updateProgress("Remaining: " + remaining / 3600 + " h");
								}
								lastProgressBarValue = f.floatValue();
							}
						}
					}
					else
					{
						((CLIApplication)getSupport().getWorkbench().getApplication())
							.updateLoadingPrompt("");
					}
				}
			}
		};
		if (EventQueue.isDispatchThread ())
		{
			r.run ();
		}
		else
		{
			EventQueue.invokeLater (r);
		}
	}
	
	private void updateProgress(String text) {
		if (getSupport().getWindow().isVisible()) {
			((CLIApplication)getSupport().getWorkbench().getApplication())
				.updateLoadingPrompt(text);
		}
	}

}
