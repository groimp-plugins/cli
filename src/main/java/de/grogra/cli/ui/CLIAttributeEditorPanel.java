package de.grogra.cli.ui;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.tree.TreeModel;

import de.grogra.pf.ui.Panel;
import de.grogra.pf.ui.TextEditor;
import de.grogra.pf.ui.Widget;
import de.grogra.pf.ui.edit.Property;
import de.grogra.pf.ui.edit.PropertyEditor;
import de.grogra.pf.ui.edit.PropertyEditorTree.PropertyNode;
import de.grogra.pf.ui.registry.PanelFactory;
import de.grogra.pf.ui.tree.UITree;
import de.grogra.pf.ui.util.WidgetAdapter;
import de.grogra.util.Described;

public class CLIAttributeEditorPanel extends CLIComponent{
	
	int iterable = 0;
	Map<String, EditableAttribute> editableAttribute = new HashMap<String, EditableAttribute>();
	
	public class EditableAttribute {
		Property property;
		PropertyEditor editor;
		Object oldValue;
		
		EditableAttribute(Property property, PropertyEditor editor, Object value){
			this.oldValue=value;
			this.property=property;
			this.editor=editor;
		}

		@Override
	    public String toString() {
	        return  '['+this.property.getType().getSimpleName()+']' 
	        		+'='+((this.property.getValue()!=null)?(this.property.getValue().toString()):"");
	    }
	}

	@Override
	public void show() {
		// unwrap the tree to get the map of editable widgets
		// Write the Map in a tmp file
		// open it with nano
		// get the output 
		// if values changed set the widget new values
		TreeModel model = getSourceTree();
		unwrapUITree((UITree)model);
		if (!editableAttribute.isEmpty()) {
			// create tmp file
			TextEditor t;
			Panel p = getSupport().getWindow().getPanel("/ui/panels/texteditor");
			if(p!=null) {
				t=(TextEditor)p;
			}else {				
				t = (TextEditor) PanelFactory.createPanel (getSupport(),"/ui/panels/texteditor", null);
			} 
			List<String> attributesLines = ((CLITextEditor) t).openAttributeEditor(editableAttribute);
			
			if (attributesLines.isEmpty()) {
				return;
			}
			for (String line : attributesLines) {
				try {
					updateAttribute(line);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		

	}
	

		
	public TreeModel getSourceTree ()
	{
		return (UITree)getContent();
	}

	private void unwrapUITree(UITree tree) {
		editableAttribute.clear();
		editableAttribute = new HashMap<String, EditableAttribute>();
		visitTree(tree, tree.getRoot());
	}
	
	private void visitTree(TreeModel model, Object object) {
		EditableAttribute eWidget = getEditableAttribute(model, object);
	    if (eWidget!=null) {
	    	editableAttribute.put(getNodeText(model, object), eWidget);
	    }
	    
	    for (int i = 0; i < model.getChildCount(object); i++) {
	    	visitTree(model, model.getChild(object, i));
	    }
	}
	
	private EditableAttribute getEditableAttribute(TreeModel model, Object item) {
		if (item instanceof PropertyNode) {
			Widget widget = null;
			Object o = ((PropertyNode)item).getWidget();
			if (o != null && o instanceof WidgetAdapter) {
				widget = (WidgetAdapter)o;
				if (widget.getComponent() instanceof CLIWidget) {
					if ( ((CLIWidget)widget.getComponent()).getEditable() ) {
						return new EditableAttribute(
								((PropertyNode)item).getProperty(),
								((PropertyNode)item).getProperty().getEditor(),
								((PropertyNode)item).getProperty().getValue());
					}
				}
			}
		}
		return null;
	}
	
	private String getNodeText(TreeModel model, Object item) {
		return String.valueOf (((UITree) model).getDescription
				   (item, Described.NAME));
	}
	
	//TODO: move to utils?
	private String getAttributeName(String line) {
		int p = line.indexOf (':');
		if (p >= 0)
		{
			return line.substring(0,p);
		}
		return null;
	}
	
	private String getAttributeValue(String line) {
		int p = line.indexOf ('=');
		if (p >= 0)
		{
			return line.substring(p+1,line.length());
		}
		return null;
	}
	
	
	public void updateAttribute(String line) throws InterruptedException {
		EditableAttribute a = editableAttribute.get( getAttributeName(line));
		if (a!=null) {
			String value = getAttributeValue(line);
			if (value!=null) {
				try {
				Object newValue = a.property.getType().valueOf(value);
				if (!a.oldValue.equals(newValue))
					a.property.setValue(newValue);
				}catch(ClassCastException e) {
					getSupport().getWorkbench().logInfo(
							"Cannot cast value for attribute :"+getAttributeName(line));
				}
			}
		}
	}
		
}
