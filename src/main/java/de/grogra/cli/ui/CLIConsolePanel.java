package de.grogra.cli.ui;

import java.awt.Color;
import java.awt.EventQueue;
import java.io.IOException;
import java.io.PipedReader;
import java.io.PipedWriter;
import java.io.Reader;
import java.io.Writer;
import java.util.Map;
import java.util.Vector;
import java.util.prefs.Preferences;

import javax.swing.UIManager;

import de.grogra.pf.ui.Command;
import de.grogra.pf.ui.ComponentWrapper;
import de.grogra.pf.ui.Console;
import de.grogra.pf.ui.NameCompletion;
import de.grogra.pf.ui.Panel;
import de.grogra.pf.ui.Window;
import de.grogra.pf.ui.Workbench;
import de.grogra.pf.ui.Console.ConsoleWriter;
import de.grogra.pf.ui.tree.UITree;
import de.grogra.util.Utils;
import de.grogra.cli.CLIApplication;
import de.grogra.pf.ui.*;

public class CLIConsolePanel extends CLIPanelSupport implements Console{

	//CONSOLE PARAMS
	private final static String LAST_COMMAND = "last-command-";
	private final static String LAST_COMMAND_COUNT = "last-command-count";
	private final	Vector<String> history = new Vector<String>();

	private final ConsoleWriterCLI out;
	private final ConsoleWriterCLI err;
	private PipedWriter inWriter;
	private final Reader in;
	
	// lock to wait notification from applciation (or workbench)
	private final Object lock = new Object();
	
	// PANEL PARAMS
	CLITextComponent console;
	Context ctx;
	de.grogra.util.Map params;
	
	
	public CLIConsolePanel(Context context, de.grogra.util.Map params) {
		super(new CLITextComponent());
		this.ctx = context;
		this.params = params;
		this.console=(CLITextComponent) getComponent();
		setContent(console);
		
		out = new ConsoleWriterCLI ();
		err = new ConsoleWriterCLI ();
		
		try {
			inWriter = new PipedWriter ();
			in = new PipedReader (inWriter);
		} catch (IOException e) {
			throw new AssertionError (e);
		}
		
		
	}
	
	@Override 
	public ComponentWrapper getContent() {
		return console;
	}

	@Override
	public void setContent(ComponentWrapper content) {
		if (content instanceof CLITextComponent) {
			if (this.console != null) {
				content.dispose();
			}
			this.console = (CLITextComponent) content;
			this.console.initialize(this, params);
		}
	}
	
	@Override
	protected void disposeImpl ()
	{
		super.disposeImpl ();
		synchronized (lock)
		{
			try
			{
				inWriter.close ();
			}
			catch (IOException e)
			{
				e.printStackTrace ();
			}
			inWriter = null;
		}
	}

	
	// CONSOLE methods

	@Override
	public void enter(String text) {
		synchronized (lock)
		{
			if (inWriter != null )
				try {
					inWriter.write(text);
					inWriter.flush();
				} catch ( IOException e	) {
					e.printStackTrace ();
				}
		}
	}

	@Override
	public void clear() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Reader getIn() {
		return in;
	}

	@Override
	public ConsoleWriterCLI getOut() {
		return out;
	}

	@Override
	public ConsoleWriterCLI getErr() {
		return err;
	}

	@Override
	public void setNameCompletion(NameCompletion nc) {
		// TODO Auto-generated method stub
		
	}

	private class WriterCLI extends Writer
	{
		private final StringBuffer buffer = new StringBuffer ();
		
		WriterCLI ()
		{
		}

		@Override
		public synchronized void write (char[] buf, int ofs, int len)
		{
			buffer.append (buf, ofs, len);
		}

		@Override
		public synchronized void flush ()
		{
			final String s = buffer.toString ();
			buffer.setLength (0);
			console.read(s);
		}

		@Override
		public void close ()
		{
		}
	}
	
	private class ConsoleWriterCLI extends ConsoleWriter
	{

		ConsoleWriterCLI ()
		{
			super(new WriterCLI(), false);
		}

		@Override
		public void print (Object text, int color)
		{
			flush ();
			print (text);
			flush ();
		}

		@Override
		public void println (Object text, int color)
		{
			flush ();
			println (text);
			flush ();
		}
	}
	
	private void initHistory() {
		Preferences p = Preferences.userRoot ().node ("/de/grogra/workbench");
		String value = p.get (LAST_COMMAND_COUNT, null);
		
		if (value==null) return;
		int n = Integer.parseInt(value);
		for(int i = 1; i<=n; i++) {
			history.add(p.get(LAST_COMMAND+i, ""));
		}
	}

}
