package de.grogra.cli.ui;

import java.awt.Component;

import javax.swing.SwingUtilities;

import de.grogra.cli.CLISynchronizer;
import de.grogra.cli.ExecutableComponent;
import de.grogra.pf.registry.*;
import de.grogra.pf.ui.*;
import de.grogra.pf.ui.util.*;
import de.grogra.pf.ui.awt.*;
import de.grogra.pf.ui.tree.*;
import de.grogra.util.*;


public class CLIPanelSupport implements Panel, RegistryContext, ExecutableComponent, Synchronizer.Callback
{
	protected CLIWindowSupport ws;
	protected ModifiableMap.Producer mapProducer;

	Panel textViewer;
	
	protected final CLISynchronizer sync = new CLISynchronizer (this);

	protected String panelId;
	private final ICLIPanel panel;

	protected UITree menu = null;
	private ComponentWrapper content = null;
	private boolean disposed = false;

	private Panel decorator;
	private final java.util.HashMap propertyMap = new java.util.HashMap ();
	
	private final Object LOCK = new Object();

	public CLIPanelSupport (CLIRootPane p) {
		this.panel = p;
	}
	
	public CLIPanelSupport (Disposable p) {
		this.panel = new CLIPanel(p);
	}

	static CLIPanelSupport get (CLIComponent c)
	{
		while (c != null)
		{
			if (c instanceof ICLIPanel)
			{
				return ((ICLIPanel) c).getSupport ();
			}
			c = c.getParent ();
		}
		return null;
	}


	public final Panel initialize (Panel ws, Map params)
	{
		((CLIWindowSupport)ws).addPanel(this);
		this.ws = (CLIWindowSupport)ws;
		panelId = (String) params.get (Panel.PANEL_ID, panelId);
		panel.initialize(this, params);
		configure (params);
		return this;
	}

	
	public void initDecorator (Panel decorator)
	{
		this.decorator = decorator;
	}


	public Panel getDecorator ()
	{
		return decorator;
	}

	
	Panel unresolve ()
	{
		Panel p = this;
		while (p.getDecorator () != null)
		{
			p = p.getDecorator ();
		}
		return p;
	}


	public java.util.Map getUIPropertyMap ()
	{
		return propertyMap;
	}


	void addParameters (ModifiableMap map)
	{
		if (mapProducer != null)
		{
			mapProducer.addMappings (map);
		}
		Panel p = this;
		while ((p = p.getDecorator ()) != null)
		{
			if (p instanceof ModifiableMap.Producer)
			{
				((ModifiableMap.Producer) p).addMappings (map);
			}
		}
	}


	protected void configure (Map params)
	{
	}


	public String getPanelId ()
	{
		return panelId;
	}


	public Workbench getWorkbench ()
	{
		return ws.getWorkbench ();
	}


	public Window getWindow ()
	{
		return ws;
	}


	public Panel getPanel ()
	{
		return this;
	}


	public Object getComponent ()
	{
		return panel;
	}


	public Registry getRegistry ()
	{
		return getWorkbench ().getRegistry ();
	}

	
	public Panel resolve ()
	{
		return this;
	}


	private static final int SET_MENU = 1;
	private static final int SET_CONTENT = 2;
	private static final int SET_VISIBLE = 3;
	protected static final int MIN_UNUSED_ACTION = 4;


	static final int MIN_PANEL_ACTION = 100;


	public Object run (int action, int iarg, Object arg, Object arg2)
	{
		switch (action)
		{
			case SET_MENU:
			{	
//				panel.setMenu ((menu != null) ? (CLIComponent) menu.getRoot ()
//							   : null);
//				ws.manager.revalidate (panel);
				
				break;
			}
			case SET_CONTENT:
			{
				if (arg instanceof CLIComponent)
					((CLIContainer)panel).add( (CLIComponent)arg );
				else if(arg instanceof ComponentWrapper)
					((CLIContainer)panel).add( (ComponentWrapper)arg );
				panel.initialize(this, null);
				break;
			}
			case SET_VISIBLE:
			{
				setVisibleSync(iarg != 0, (Panel) arg, Boolean.TRUE.equals (arg2));
				break;
			}
			default:
				if ((action < MIN_PANEL_ACTION)
						|| !(panel instanceof AWTSynchronizer.Callback))
					{
						throw new AssertionError ("Illegal action code: " + action);
					}
					return ((AWTSynchronizer.Callback) panel)
						.run (action, iarg, arg, arg2);
		}
		return null;
	}
	
	void setVisibleSync (boolean visible, Panel keepInFront, boolean moveToFront)
	{
		if (visible)
		{
				if (moveToFront && getContent()!=null) {
					((CLIComponent)getContent().getComponent()).show();
			}
		}
	}


	public void setMenu (UITree t)
	{
		if (menu != null)
		{
			menu.dispose ();
			menu = null;
		}
		if (t != null)
		{
			menu =  t;
		}
		sync.invokeAndWait (SET_MENU);
	}


	public UITree getMenu ()
	{
		return menu;
	}


	public void setContent (ComponentWrapper content)
	{
		if (this.content != null)
		{
			this.content.dispose ();
		}
		this.content = content;
		sync.invokeAndWait(SET_CONTENT, 0, (content != null) ? content.getComponent () : null, null);
	}

	
	public ComponentWrapper getContent ()
	{
		return content;
	}


	public void dispose ()
	{
		if (disposed)
		{
			return;
		}
		disposed = true;

		setMenu (null);
		setContent (null);
		disposeImpl ();
	}


	public final void show (boolean moveToFront, Panel keepInFront)
	{
		sync.invokeAndWait (SET_VISIBLE, 1, keepInFront, moveToFront);
	}


	public final void hide ()
	{
		sync.invokeAndWait (SET_VISIBLE, 0, null, null);
	}

	protected void disposeImpl ()
	{
		panel.dispose ();
	}

	
	void installUpdater (UIProperty property, boolean forComponent, String method, Map params)
	{
		UIPropertyUpdater.install (property, this, forComponent, method,
								   AWTSynchronizer.QUEUE, params);
	}

	
	private final class CloseHelper implements Command
	{
		private final Runnable ok;
		
		CloseHelper (Runnable ok)
		{
			this.ok = ok;
		}

		public void run (Object info, Context context)
		{
			if (ok == null)
			{
				checkClose ((Command) info);
			}
			else
			{
				try
				{
					java.awt.EventQueue.invokeAndWait (ok);
				}
				catch (Exception e)
				{
					context.getWorkbench ().logInfo ("Exception", e);
				}
			}
		}

		public String getCommandName ()
		{
			return null;
		}
	}


	private final Command CHECK_CLOSE = new CloseHelper (null);
	

	public void checkClose (Runnable ok)
	{
		ok.run ();
	}


	protected void executeCheckClose (Runnable ok)
	{
		getWorkbench ().getJobManager ().execute
			(CHECK_CLOSE, new CloseHelper (ok), this, JobManager.ACTION_FLAGS);
	}
	

	public void checkClose (Command ok)
	{
		ok.run (null, this);
	}


	@Override
	public String toString ()
	{
		return super.toString () + "[panelId=" + getPanelId () + ']';
	}


	@Override
	public void setCursor(int cursor) {
		// TODO Auto-generated method stub
		
	}
	
	
	/**
	 * Push the given string on the content of the panel.
	 * Some CLI Components can implement a run (String) method to execute a specific
	 * command.
	 * 
	 * @param cmd
	 */
	public void run(String cmd, Object info) {
		switch (cmd) {
		case ("close"):
			((CLIWindowSupport)getWindow()).removePanel(getPanelId());
			dispose();
			break;
		default:
			if (getContent() instanceof ExecutableComponent) {
				((ExecutableComponent)getContent()).run(cmd,info);
			}
			//TODO: shouldn't go there (all component should be wrapped into getContent > CLIComponent)
			else if (getContent().getComponent() instanceof ExecutableComponent) {
				((ExecutableComponent)getContent().getComponent()).run(cmd,info);
			}
			break;
		}
		
	}
}
