package de.grogra.cli.completer;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Supplier;

import org.jline.builtins.Styles;
import org.jline.reader.Candidate;
import org.jline.reader.LineReader;
import org.jline.reader.ParsedLine;
import org.jline.terminal.Terminal;
import org.jline.utils.AttributedString;
import org.jline.utils.AttributedStringBuilder;
import org.jline.utils.OSUtils;
import org.jline.utils.StyleResolver;

public class RegistryItemCompleter implements org.jline.reader.Completer{
	
	private Collection<String> commands;
	
    public RegistryItemCompleter(String... strings) {
        this(Arrays.asList(strings));
    }

    public RegistryItemCompleter(Iterable<String> strings) {
        assert strings != null;
        this.commands = new ArrayList<>();
        for (String string : strings) {
        	commands.add(string);
        }
    }
    
    public void setCommands(Iterable<String> strings) {
    	assert strings != null;
        this.commands = new ArrayList<>();
        for (String string : strings) {
        	commands.add(string);
        }
    }
	
	public void complete(LineReader reader, ParsedLine commandLine, final List<Candidate> candidates) {
        assert commandLine != null;
        assert candidates != null;
        
        Set<String> toAdd = new HashSet<String>();

        String buffer = commandLine.word().substring(0, commandLine.wordCursor());
        String current;
        String curBuf;
        String sep = "/";
        int lastSep = buffer.lastIndexOf(sep);
        try {
            if (lastSep >= 0) {
                curBuf = buffer.substring(0, lastSep + 1);
            } else {
                curBuf = "";
            }
//            System.out.println("BUFF " + curBuf);
            
            for (String cmd : commands) {
            	if (accept(cmd, curBuf)) {
            		String futureCandidate = cmd.substring(curBuf.toString().length());
            		int cmdLastSep = futureCandidate.indexOf(sep);
            		if (cmdLastSep >= 0) {
            			futureCandidate = futureCandidate.substring(0, cmdLastSep+1);
            		}
            		toAdd.add(futureCandidate);
            	}
            }
            for (String c : toAdd) {
            	if (c.endsWith(sep)) {
            		candidates.add(new Candidate(curBuf+c, c,null, null, sep, null, false));
            	}
            	else
            		candidates.add(new Candidate(curBuf+c, c,null, null, null, null, true));
            }
        }catch(Exception e) {}
    }

    public Path getRootDir() {
		return Paths.get("");
	}
	
	protected boolean accept(String isIn, String path) {
		return isIn.startsWith(path);
    }
}