package de.grogra.cli.completer;


import de.grogra.cli.CLIApplication;
import de.grogra.cli.Utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

import org.jline.reader.Completer;
import org.jline.reader.impl.completer.ArgumentCompleter;
import org.jline.reader.impl.completer.NullCompleter;
import org.jline.reader.impl.completer.StringsCompleter;

public class WindowCompleter extends ArgumentCompleter{
	
	//list of opened panels
//	WindowPanelCompleter listPanels;
	
	public WindowCompleter(CLIApplication app) {
		
		setStrict(true);
		
		List<String> runCmd = new ArrayList<String>();
		
		// the default run that all panels get
		runCmd.add(":display");
		runCmd.add(":hide");
		runCmd.add(":close");
		runCmd.add(":repaint");
		runCmd.add(":show");
		runCmd.add(":select");
		
		StringsCompleter arguments = new StringsCompleter(runCmd);
		
		List<String> cmdWindow = Utils.getWindowCommands(app);
		RegistryItemCompleter commands = new RegistryItemCompleter(cmdWindow);
		
		this.getCompleters().addAll(Arrays.asList(commands, arguments, NullCompleter.INSTANCE));
				
	}
	
}
