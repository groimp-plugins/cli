package de.grogra.cli;

import java.awt.Dimension;
import java.awt.HeadlessException;
import java.awt.LayoutManager;
import java.awt.Point;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.LogRecord;

import javax.swing.JTree;
import javax.swing.ListModel;
import javax.swing.table.TableModel;
import de.grogra.cli.ui.*;
import de.grogra.icon.IconSource;
import de.grogra.pf.ui.tree.UITree;
import de.grogra.pf.ui.util.ComponentWrapperImpl;
import de.grogra.reflect.Type;
import de.grogra.util.Disposable;
import de.grogra.util.Map;
import de.grogra.util.Quantity;
import de.grogra.util.Utils;
import de.grogra.pf.ui.*;
import de.grogra.cli.CLIApplication;

public class CLIToolkit extends UIToolkit
{

	@Override
	public Panel createPanel (Context ctx, Disposable toDispose, Map params)
	{
		return new CLIPanelSupport (toDispose)
				.initialize ((CLIWindowSupport) ctx.getWindow (), params);
	}

	@Override
	public Console createConsole (Context context, Map params)
	{
		return (Console)new CLIConsolePanel(context, params).initialize(context.getWindow(), params);
	}
	
	@Override
	public Panel createStatusBar (Context context, Map params)
	{
//		return (Panel)new CLIPanel(null);
		
		CLIStatusBar bar = new CLIStatusBar();
		bar.initialize((CLIWindowSupport) context.getWindow (), params);
		Panel p = createPanel (context, null, params);
		p.setContent (bar);
		return p;
	}

	@Override
	public Window createWindow (Command close, Map params)
	{
		return new CLIWindowSupport (this, close, params);
	}

	@Override
	public Panel createViewerPanel (Context ctx, URL url, Map params)
	{
		return (Panel)new CLIPanel(null).getSupport();
	}

	@Override
	public TextEditor createTextEditor (Context context, Map params)
	{
		CLITextEditor ed = new CLITextEditor();
		ed.initialize((CLIPanelSupport)context.getWindow(), params);
		return ed;
	}

	@Override
	public Panel createToolBar (Context context, Map params)
	{
		return (Panel)new CLIPanel(null).getSupport();
	}

	@Override
	public Object createTextViewer (URL url, String mimeType, String content, Command hyperlink, boolean asBrowser)
	{
		CLITextComponent c = new CLITextComponent();
//		c.initialize((CLIPanelSupport)Workbench.current().getWindow(), Map.EMPTY_MAP);
		return c;
	}

	@Override
	public void setContent (Object textViewer, String mimeType, String content)
	{
		((CLITextComponent)textViewer).read(content);

	}

	@Override
	public void setContent (Object textViewer, URL content)
	{
		Workbench.current().logInfo(CLIApplication.I18N.msg("error.url"));
	}

	@Override
	public Object createLabel (String text, IconSource icon, Dimension size, int flags)
	{
		CLIComponent c = new CLIComponent();
		c.setName(text);
		return c;
	}

	@Override
	public Object createButton (String text, IconSource source, Dimension size, int flags, Command cmd, Context ctx)
	{
		CLIComponent c = new CLIComponent();
		c.setName(text);
		return c;
	}

	@Override
	public Object createLabeledComponent (Object component, Object label)
	{
		CLIComponent c = new CLIComponent();
		c.setName(label.toString());
		return c;
	}

	@Override
	public Widget createNumericWidget (Type type, Quantity quantity, Map params)
	{
		return new CLIWidget(type, quantity, params);
	}

	@Override
	public Widget createStringWidget (Map params)
	{
		return new CLIWidget("String");
	}

	@Override
	public Widget createTreeChoiceWidget (UITree tree)
	{
		return new CLIWidget(tree);
	}

	@Override
	public Widget createChoiceWidget (ListModel list, boolean forMenu)
	{
		return new CLIWidget(list, forMenu);
	}

	@Override
	public Widget createBooleanWidget (boolean forMenu, Map params)
	{
		return new CLIWidget(forMenu, params);
	}

	@Override
	public Widget createColorWidget (Map params)
	{
		return new CLIWidget("Color");
	}

	@Override
	public ChartPanel createChartPanel (Context ctx, Map params)
	{
//		Workbench.current().logInfo(CLIApplication.I18N.msg("error.chartpanel"));
		return (ChartPanel) (new CLIChartPanelSupport(new CLIPanel(null)))
				.initialize((CLIPanelSupport)ctx.getWindow(), params);
	}

	@Override
	public Object createScrollPane (Object view)
	{
		return new CLIContainer ((ComponentWrapper) view);
	}

	@Override
	public Object createTabbedPane (String[] titles, Object[] components)
	{
		CLIComponent c = new CLIComponent();
		c.setName(titles.toString());
		c.setContent(components);
		return c;
	}

	@Override
	public Object createContainer (int gap)
	{
		return new CLIContainer ();
	}

	@Override
	public Object createSplitContainer (int orientation)
	{
		return new CLIContainer ();
	}

	@Override
	public Object createContainer (int rows, int cols, int gap)
	{
		return new CLIContainer ();
	}

	@Override
	public Object createContainer (float[] weights, int gap)
	{
		return new CLIContainer ();
	}

	@Override
	public Object setBorder (Object component, int gap)
	{
		return null;
	}

	@Override
	public void addComponent (Object container, Object component, Object constraints, int index)
	{
		if (!(component instanceof CLIComponent)) {
			CLIComponent c = new CLIComponent();
			c.setContent(component);
			((CLIContainer)container).add((CLIComponent)c, constraints, index);
		}else {
			((CLIContainer)container).add((CLIComponent)component, constraints, index);
		}
		
	}

	@Override
	public void removeComponent (Object component)
	{
		if (component != null)
		{
			final CLIComponent c = (CLIComponent) component;
			c.getParent ().remove (c);
		}
	}

	@Override
	public Object getParent (Object component)
	{
		return ((CLIComponent)component).getParent();
	}

	@Override
	public int indexOf (Object component)
	{
		return 0;
	}

	@Override
	public int getComponentCount (Object container)
	{
		return ((CLIContainer)container).countComponents();
	}

	@Override
	public Object getComponent (Object container, int index)
	{
		return ((CLIContainer)container).getComponent(index);
	}

	@Override
	public Point getLocationOnScreen (Object component)
	{
		throw new HeadlessException ();
	}

	@Override
	public int getWidth (Object component)
	{
		throw new HeadlessException ();
	}

	@Override
	public int getHeight (Object component)
	{
		throw new HeadlessException ();
	}

	@Override
	public void revalidate (Object component)
	{
	}

	@Override
	public void repaint (Object component)
	{
	}

	@Override
	public Object getTextViewerComponent (Panel viewerPanel)
	{
		return new CLITextComponent();
	}


	@Override
	public ComponentWrapper createComponentTree (UITree componentTree)
	{
		CLIAttributeEditorPanel c = new CLIAttributeEditorPanel();
		c.setContent(componentTree);
		return new ComponentWrapperImpl(c, componentTree);
	}

	@Override
	public ComponentWrapper createComponentMenu (UITree componentTree)
	{
		return new CLIMenuComponent(componentTree);
	}

	@Override
	public ComponentWrapper createTable (TableModel table, Context ctx)
	{
		throw new HeadlessException ();
	}

	@Override
	public int getSelectedRow (ComponentWrapper table)
	{
		throw new HeadlessException ();
	}

	@Override
	public TableModel getTable (ComponentWrapper table)
	{
		throw new HeadlessException ();
	}

	@Override
	public void showPopupMenu (UITree menu, Object component, int x, int y)
	{
		throw new HeadlessException ();
	}

	@Override
	public ComponentWrapper createTreeInSplit(UITree tree, Object split) {
		CLIUITree st = (CLIUITree) createTree (tree);
		return st;
	}

	@Override
	public ComponentWrapper createTree (UITree tree)
	{
		return new CLIUITree(tree);
	}

	/**
	 * Transform a JTree into a component wrapper with the model tree as component
	 */
	@Override
	public ComponentWrapper createUITreeComponent(Object componentTree) {
		return new CLIUITree((UITree)((JTree)componentTree).getModel());
	}

	@Override
	public void setLayout(Object component, LayoutManager layout) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Panel createLogViewer(Context ctx, Map params) {
		class CLILoggingHandler extends LoggingHandler {

			public CLILoggingHandler(Context ctx) {
				super(ctx);
			}
			@Override
			public synchronized void publish (LogRecord record)
			{
				if (groupingCount == 0)
				{
					pendingRecords.add (record);
					publish ();
				}
				else
				{
					groupedRecords.add (record);
				}
			}
			
		}
		
		final LoggingHandler handler = new LoggingHandler (ctx);
		handler.setFormatter
			(new CLILoggingFormatter (UI.I18N, UIToolkit.MEDIUM_ICON_SIZE));
		handler.setMimeType ("text/html");
		handler.setLevel (Level.ALL);
		Object o = Utils.get (params, "logrecords", null);
		if (o instanceof LogRecord[][])
		{
			LogRecord[][] a = (LogRecord[][]) o;
			for (int i = 0; i < a.length; i++)
			{
				LogRecord[] b = a[i];
				if ((b != null) && (b.length > 0))
				{
					handler.beginGrouping ();
					for (int j = 0; j < b.length; j++)
					{
						handler.publish (b[j]);
					}
					handler.endGrouping ();
				}
			}
		}

		class Helper implements ComponentWrapper, JobManager.ExecutionListener
		{
			private boolean executing;

			public void dispose ()
			{
				UI.getJobManager (ctx).removeExecutionListener (this);
				ctx.getWorkbench ().getLogger ().removeHandler (handler);
			}
			
			public Object getComponent ()
			{
				return handler.getComponent ();
			}

			public void executionStarted (JobManager jm)
			{
				executing = true;
				handler.beginGrouping ();
			}

			public void executionFinished (JobManager jm)
			{
				if (executing)
				{
					executing = false;
					handler.endGrouping ();
				}
			}
		}

		ctx.getWorkbench ().getLogger ().addHandler (handler);
		Helper h = new Helper ();
		UI.getJobManager (ctx).addExecutionListener (h);

		Panel p = createPanel (ctx, null, params);
		p.setContent (h);
		return p;
	}

	
}
